package main

import (
	"belo/src/handlers"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	_ "github.com/joho/godotenv/autoload"
)

func main() {
	r := mux.NewRouter()
	r.Use(jsonMiddleware)
	r.HandleFunc("/status", handlers.HandlePing).Methods(http.MethodGet)
	r.HandleFunc("/estimation", handlers.HandleGetEstimation).Methods(http.MethodPost)
	r.HandleFunc("/estimation/{id}", handlers.HandleMakeSwap).Methods(http.MethodPost)

	fmt.Printf("Init belo API port: %s", os.Getenv("PORT"))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("PORT")), r))
}

func jsonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}
