-- Drop table

DROP TABLE IF EXISTS public.estimations;

CREATE TABLE public.estimations (
	id varchar NOT NULL,
	pair varchar NOT NULL,
	amount float8 NULL,
	fees float8 NULL,
	total float8 NULL,
	expiration int8 NULL,
	executed bool NULL DEFAULT false,
	executedtime int8 NULL,
	orderid varchar NULL,
	CONSTRAINT estimations_pkey PRIMARY KEY (id)
);



-- Drop table

DROP TABLE IF EXISTS public.fees;

CREATE TABLE public.fees (
	"name" varchar NOT NULL,
	value float8 NOT NULL,
	CONSTRAINT fees_pkey PRIMARY KEY (name)
);

INSERT INTO fees ("name", value)  VALUES ('belo', 1.0)