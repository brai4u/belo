# Belo API

## Requerimientos

* Git
* Docker
* Okex **DEMO** api key (https://www.okex.com/docs-v5/en/#demo-trading-explorer)

## Instalacion

```
git clone git@github.com:brianardiles/belo.git
```

```
cp .env.example .env
```

```
docker compose build
```

```
docker compose up
```

## Configurar belo fees
Cambiar en la tabla fees de la base de datos beloapp el value del registro "belo", por defecto el valor es 1%

## Endpoints

**Check Status**

```
GET localhost:8080/status
```

Response

```json
{
    "status": "Ok",
    "timestamp": 123
}
```

**Make estimation**

```
POST localhost:8080/estimation
```

*body*
```
{
    "pair": "BTC-USDT",
    "amount": 47000
}
```

*response*
```
{
    "id": "b21b66d7-bc76-4fb5-ae02-dddfd42b1c4b",
    "pair": "BTC-USDT",
    "amount": 47000,
    "fees": 470.235,
    "total": 0.957757296941254,
    "expiration": 1639669254,
    "executed": false,
    "executedTime": 0,
    "orderId": ""
}
```

**Make swap**

```
POST localhost:8080/estimation/{estimationID}
```

*response OK*
```
{
    "id": "3f278c65-2981-496c-b5d3-3d058796fd51",
    "pair": "BTC-USDT",
    "amount": 47000,
    "fees": 470.235,
    "total": 0.9583885684860968,
    "expiration": 1639669287,
    "executed": true,
    "executedTime": 1639669267,
    "orderId": "391736532612296704"
}
```

*response Estimacion expirada*
```
{
    "error": "estimation expired"
}
```

*response Estimacion ya executada*
```
{
    "error": "estimation already executed"
}
```

*response Estimacion no existe*
```
{
    "error": "estimation not found"
}
```