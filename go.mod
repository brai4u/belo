module belo

go 1.16

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/lib/pq v1.10.4 // indirect
	moul.io/http2curl v1.0.0 // indirect
)
