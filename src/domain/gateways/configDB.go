package gateways

type ConfigDB interface {
	GetFees() float64
}
