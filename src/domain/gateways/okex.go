package gateways

import "belo/src/domain/dto"

type Okex interface {
	GetOrderBookByPair(request dto.EstimateInput) (dto.OrderBook, error)
	GetFees(pair string) (float64, error)
	MakeSwap(estimation dto.Estimation) (dto.SwapResponse, error)
}
