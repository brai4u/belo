package gateways

import "belo/src/domain/dto"

type Estimations interface {
	GetEstimationById(ID string) (dto.Estimation, bool, error)
	SaveEstimation(estimation dto.Estimation) error
	UpdateEstimation(estimation dto.Estimation) error
}
