package domain

import (
	"belo/src/domain/dto"
	"belo/src/domain/gateways"
	"fmt"
	"log"
	"time"

	"github.com/google/uuid"
)

type GetEstimation struct {
	ExchangeRepo    gateways.Okex
	ConfigRepo      gateways.ConfigDB
	EstimationsRepo gateways.Estimations
}

func NewGetEstimation(exchangeRepo gateways.Okex, config gateways.ConfigDB, estimationsRepo gateways.Estimations) GetEstimation {
	return GetEstimation{ExchangeRepo: exchangeRepo, ConfigRepo: config, EstimationsRepo: estimationsRepo}
}

func (useCase GetEstimation) Run(request dto.EstimateInput) (dto.Estimation, error) {
	id := uuid.New()
	orderbook, err := useCase.ExchangeRepo.GetOrderBookByPair(request)
	if err != nil {
		log.Printf("Error getting order book %+v", err)
		return dto.Estimation{}, err
	}

	beloFees := useCase.ConfigRepo.GetFees()
	exchangeFees, err := useCase.ExchangeRepo.GetFees(request.Pair)
	if err != nil {
		log.Printf("Error getting exchange fees %+v", err)
		return dto.Estimation{}, fmt.Errorf("error getting exchange fees")
	}
	fees := (request.Amount * beloFees / 100) + (request.Amount * exchangeFees / 100)
	total := (request.Amount - fees) / orderbook.Price

	estimation := dto.Estimation{
		ID:         id.String(),
		Pair:       request.Pair,
		Amount:     request.Amount,
		Fees:       fees,
		Total:      total,
		Expiration: time.Now().Add(30 * time.Second).Unix(),
	}

	err = useCase.EstimationsRepo.SaveEstimation(estimation)
	if err != nil {
		log.Printf("Error saving estimation %+v", err)
		return dto.Estimation{}, fmt.Errorf("error saving estimation")
	}

	return estimation, nil
}
