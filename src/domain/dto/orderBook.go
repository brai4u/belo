package dto

type OrderBook struct {
	Price            float64
	Size             float64
	LiquidatedOrders int
	Orders           int
}
