package dto

type EstimateInput struct {
	Pair   string
	Amount float64
}
