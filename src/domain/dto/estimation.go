package dto

type Estimation struct {
	ID           string  `json:"id"`
	Pair         string  `json:"pair"`
	Amount       float64 `json:"amount"`
	Fees         float64 `json:"fees"`
	Total        float64 `json:"total"`
	Expiration   int64   `json:"expiration"`
	Executed     bool    `json:"executed"`
	ExecutedTime int64   `json:"executedTime"`
	OrderID      string  `json:"orderId"`
}
