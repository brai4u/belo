package dto

type FeesResponseOkex struct {
	Code string `json:"code"`
	Data []struct {
		Category string `json:"category"`
		Delivery string `json:"delivery"`
		Exercise string `json:"exercise"`
		InstType string `json:"instType"`
		Level    string `json:"level"`
		Maker    string `json:"maker"`
		Taker    string `json:"taker"`
		Ts       string `json:"ts"`
	} `json:"data"`
	Msg string `json:"msg"`
}
