package domain

import (
	"belo/src/domain/dto"
	"belo/src/domain/gateways"
	"fmt"
	"log"
	"time"
)

type MakeSwap struct {
	ExchangeRepo    gateways.Okex
	EstimationsRepo gateways.Estimations
}

func NewMakeSwap(exchangeRepo gateways.Okex, estimationsRepo gateways.Estimations) MakeSwap {
	return MakeSwap{ExchangeRepo: exchangeRepo, EstimationsRepo: estimationsRepo}
}

func (useCase MakeSwap) Run(estimationID string) (dto.Estimation, error) {

	estimation, found, err := useCase.EstimationsRepo.GetEstimationById(estimationID)
	if err != nil {
		log.Printf("Error getting estimation by id estimationID %s - error %+v", estimationID, err)
		return dto.Estimation{}, fmt.Errorf("error getting estimation")
	}

	if !found {
		log.Printf("Estimation not found estimationID %s", estimationID)
		return dto.Estimation{}, fmt.Errorf("estimation not found")
	}

	if estimation.Executed {
		log.Printf("Estimation already executed estimationID %s", estimationID)
		return dto.Estimation{}, fmt.Errorf("estimation already executed")
	}

	if estimation.Expiration < time.Now().Unix() {
		log.Printf("Estimation expired estimationID %s", estimationID)
		return dto.Estimation{}, fmt.Errorf("estimation expired")
	}

	swapResponse, err := useCase.ExchangeRepo.MakeSwap(estimation)
	if err != nil {
		log.Printf("Error making swap from estimationID %s - error %+v", estimationID, err)
		return dto.Estimation{}, fmt.Errorf("error making swap")
	}

	if swapResponse.Code != "0" {
		log.Printf("Error making swap from estimationID %s - error %+v", estimationID, err)
		return dto.Estimation{}, fmt.Errorf("error making swap")
	}

	estimation.ExecutedTime = time.Now().Unix()
	estimation.Executed = true
	estimation.OrderID = swapResponse.Data[0].OrdID

	err = useCase.EstimationsRepo.UpdateEstimation(estimation)

	if err != nil {
		log.Printf("Error updating estimationID %s - error %+v", estimationID, err)
		return dto.Estimation{}, fmt.Errorf("swap OK error updating register")
	}

	return estimation, err
}
