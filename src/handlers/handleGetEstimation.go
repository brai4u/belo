package handlers

import (
	"belo/src/domain"
	"belo/src/domain/dto"
	"belo/src/repositories"
	"belo/src/utils"
	"encoding/json"
	"net/http"
)

func HandleGetEstimation(w http.ResponseWriter, r *http.Request) {
	var request dto.EstimateInput
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		w.WriteHeader(http.StatusConflict)
		json.NewEncoder(w).Encode(map[string]string{"error": "Invalid JSON"})
		return
	}
	db := utils.NewDB()
	exchangeRepo := repositories.NewOkexRepository(&http.Client{})
	configRepo := repositories.NewConfigRepository(db)
	estimationsRepo := repositories.NewEstimationsRepository(db)

	useCase := domain.NewGetEstimation(exchangeRepo, configRepo, estimationsRepo)

	estimation, err := useCase.Run(request)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(map[string]string{"error": err.Error()})
		return
	}

	json.NewEncoder(w).Encode(estimation)
}
