package handlers

import (
	"encoding/json"
	"net/http"
	"time"
)

func HandlePing(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(map[string]interface{}{"status": "Ok", "timestamp": time.Now().Unix()})
}
