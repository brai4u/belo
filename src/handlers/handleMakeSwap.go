package handlers

import (
	"belo/src/domain"
	"belo/src/repositories"
	"belo/src/utils"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

func HandleMakeSwap(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	estimationID := vars["id"]
	if estimationID == "" {
		w.WriteHeader(http.StatusConflict)
		json.NewEncoder(w).Encode(map[string]string{"error": "Estimation ID is required"})
		return
	}

	db := utils.NewDB()
	exchangeRepo := repositories.NewOkexRepository(&http.Client{})
	estimationsRepo := repositories.NewEstimationsRepository(db)

	useCase := domain.NewMakeSwap(exchangeRepo, estimationsRepo)

	estimation, err := useCase.Run(estimationID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(map[string]string{"error": err.Error()})
		return
	}

	json.NewEncoder(w).Encode(estimation)
}
