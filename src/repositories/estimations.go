package repositories

import (
	"belo/src/domain/dto"
	"database/sql"
	"log"
)

type EstimationsRepository struct {
	DB *sql.DB
}

func NewEstimationsRepository(db *sql.DB) EstimationsRepository {
	return EstimationsRepository{DB: db}
}

func (repo EstimationsRepository) GetEstimationById(ID string) (dto.Estimation, bool, error) {
	var estimation dto.Estimation

	err := repo.DB.QueryRow(`
		SELECT id,
			pair,
			amount,
			fees,
			total,
			expiration,
			executed
		FROM public.estimations
		WHERE id = $1`, ID).
		Scan(&estimation.ID,
			&estimation.Pair,
			&estimation.Amount,
			&estimation.Fees,
			&estimation.Total,
			&estimation.Expiration,
			&estimation.Executed)

	if err != nil {
		if err == sql.ErrNoRows {
			return dto.Estimation{}, false, nil
		}

		log.Printf("error getting estimation - error %+v", err)
		return dto.Estimation{}, false, err
	}

	return estimation, true, nil
}

func (repo EstimationsRepository) SaveEstimation(estimation dto.Estimation) error {
	var lastInsertId string
	err := repo.DB.QueryRow("INSERT INTO estimations (id, pair, amount, fees, total, expiration) VALUES($1, $2, $3, $4, $5, $6) returning id;",
		estimation.ID,
		estimation.Pair,
		estimation.Amount,
		estimation.Fees,
		estimation.Total,
		estimation.Expiration).Scan(&lastInsertId)

	if err != nil {
		log.Printf("error saving estimation - error %+v", err)
		return err
	}

	return nil
}

func (repo EstimationsRepository) UpdateEstimation(estimation dto.Estimation) error {

	var lastInsertId string
	err := repo.DB.QueryRow("UPDATE estimations SET executed = $1, executedTime = $2, orderId = $3 WHERE id = $4 returning id;",
		estimation.Executed,
		estimation.ExecutedTime,
		estimation.OrderID,
		estimation.ID).Scan(&lastInsertId)

	if err != nil {
		log.Printf("error getting estimation - error %+v", err)
		return err
	}

	return nil
}
