package repositories

import (
	"belo/src/domain/dto"
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
	"os"
	"strconv"
	"time"
)

type OkexRepository struct {
	Client *http.Client
}

func NewOkexRepository(client *http.Client) OkexRepository {
	return OkexRepository{Client: client}
}

func (repo OkexRepository) GetFees(pair string) (float64, error) {
	path := fmt.Sprintf("/api/v5/account/trade-fee?instType=SWAP&uly=%s", pair)
	url := fmt.Sprintf("%s%s", os.Getenv("OKEX_DOMAIN"), path)

	resp, err := repo.makeApicall("GET", url, path, nil)
	if err != nil {
		log.Printf("error getting fees from okex - error: %+v", err)
		return 0, err
	}

	var feesResponseOkex dto.FeesResponseOkex
	json.NewDecoder(resp.Body).Decode(&feesResponseOkex)
	var fees float64
	if f, err := strconv.ParseFloat(feesResponseOkex.Data[0].Taker, 64); err == nil {
		fees = math.Abs(f)
	}

	return fees, nil
}

func (repo OkexRepository) GetOrderBookByPair(request dto.EstimateInput) (dto.OrderBook, error) {
	url := fmt.Sprintf("%s/api/v5/market/books?instId=%s&?sz=%f", os.Getenv("OKEX_DOMAIN"), request.Pair, request.Amount)
	resp, err := repo.Client.Get(url)

	if err != nil {
		log.Printf("error creating request url: %s - error: %+v", url, err)
		return dto.OrderBook{}, err
	}

	if resp.StatusCode != http.StatusOK {
		log.Printf("error getting order book url: %s - error: %+v", url, err)
		return dto.OrderBook{}, fmt.Errorf("error getting order book")
	}

	var orderBookResponseOkex dto.OrderBookResponseOkex
	json.NewDecoder(resp.Body).Decode(&orderBookResponseOkex)

	if orderBookResponseOkex.Code != "0" {
		log.Printf("error getting order book url: %s - error code: %s", url, orderBookResponseOkex.Code)
		return dto.OrderBook{}, fmt.Errorf("%s", orderBookResponseOkex.Msg)
	}

	var orderBook dto.OrderBook
	if price, err := strconv.ParseFloat(orderBookResponseOkex.Data[0].Bids[0][0], 64); err == nil {
		orderBook.Price = price
	}

	return orderBook, nil
}

func (repo OkexRepository) MakeSwap(estimation dto.Estimation) (dto.SwapResponse, error) {
	path := "/api/v5/trade/order"
	url := fmt.Sprintf("%s%s", os.Getenv("OKEX_DOMAIN"), path)

	bodyJson, err := json.Marshal(dto.SwapRequest{
		InstID:  estimation.Pair,
		TdMode:  "cash",
		Side:    "buy",
		OrdType: "ioc",
		Px:      "1",
		Sz:      fmt.Sprintf("%f", estimation.Total),
	})

	if err != nil {
		log.Printf("error marshal json - error: %+v", err)
		return dto.SwapResponse{}, err
	}

	resp, err := repo.makeApicall("POST", url, path, bodyJson)
	if err != nil {
		log.Printf("error making trade order on okex - error: %+v", err)
		return dto.SwapResponse{}, err
	}

	var swapResponse dto.SwapResponse
	json.NewDecoder(resp.Body).Decode(&swapResponse)

	if swapResponse.Code != "0" {
		log.Printf("error making swap url: %s - error code: %s", url, swapResponse.Code)
		return dto.SwapResponse{}, fmt.Errorf("%s", swapResponse.Msg)
	}

	return swapResponse, nil
}

func (repo OkexRepository) signRequest(method, path, timestamp, body string) string {
	mac := hmac.New(sha256.New, []byte(os.Getenv("OKEX_SECRET_KEY")))
	phrase := fmt.Sprintf("%s%s%s%s", timestamp, method, path, body)
	mac.Write([]byte(phrase))

	return base64.StdEncoding.EncodeToString(mac.Sum(nil))
}

func (repo OkexRepository) makeApicall(method, url, path string, body []byte) (*http.Response, error) {
	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		log.Printf("error creating request url: %s - error: %+v", url, err)
		return &http.Response{}, err
	}

	utcTime := time.Now().UTC()
	iso := utcTime.String()
	isoBytes := []byte(iso)
	timestamp := string(isoBytes[:10]) + "T" + string(isoBytes[11:23]) + "Z"

	req.Header = http.Header{
		"OK-ACCESS-KEY":        []string{os.Getenv("OKEX_API_KEY")},
		"OK-ACCESS-SIGN":       []string{repo.signRequest(method, path, timestamp, string(body))},
		"OK-ACCESS-TIMESTAMP":  []string{timestamp},
		"OK-ACCESS-PASSPHRASE": []string{os.Getenv("OK_ACCESS_PASSPHRASE")},
		"Content-Type":         []string{"application/json"},
		"x-simulated-trading":  []string{"1"},
	}

	res, err := repo.Client.Do(req)
	if err != nil {
		log.Printf("error executing request url: %s - error: %+v", url, err)
		return &http.Response{}, err
	}

	return res, nil
}
