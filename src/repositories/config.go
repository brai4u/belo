package repositories

import (
	"database/sql"
	"log"
)

type ConfigRepository struct {
	DB *sql.DB
}

func NewConfigRepository(db *sql.DB) ConfigRepository {
	return ConfigRepository{DB: db}
}

func (repo ConfigRepository) GetFees() float64 {
	var fees float64
	err := repo.DB.QueryRow(`
		SELECT value
		FROM fees
		WHERE name = $1`, "belo").
		Scan(&fees)

	if err != nil {
		log.Printf("error getting fees - error %+v", err)
		return 0
	}

	return fees
}
